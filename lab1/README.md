# Lab 1: Running a Serverless Function

Before you start, here's where to make the separate **work** tab/window if you haven't already done so. Go ahead and duplicate this tab/window. Designate one you will keep these insructions in, and one you will do all the clicking around and editing in. . . . Ok? Good, let's move on.

## Serverless function definition

For this lab, we've already put the function you will run in the repository. But let's take a look at it and understand what's in it.

We'll use GitLab's built-in Web IDE to navigate and view files. To view the file, in the upper right corner of your **work** tab click on the **Web IDE** button:

![go_to_webide](images/go_to_webide.png)

Once in the GitLab web IDE, navigate to the `hello` directory and view the `hello.py` file:

![navigate_hello](images/navigate_hello.png)

Looking at this function, we see that it starts by importing some needed libraries:

```python
import json
import datetime
```

Then it creates a method called **endpoint** which returns the current time (the name "endpoint" here is arbitrary. We could have called it "foo" as long as our later code used that name also):

```python
def endpoint(event, context):
    current_time = datetime.datetime.now().time()
    body = {
        "message": "Hello, the current time is " + str(current_time)
    }

    return body
```
That is it for our simple "hello world" function definition.

## Serverless function metadata (serverless.yml)

So, now we have written a function! That's it right?

Well, not quite.

We also need to provide some metadata about the function such as the function's
name and runtime. This is done in the `serverless.yml` file in the **root** directory of our repository.

In the repository **root** directory view the `serverless.yml` file:

![navigate_serverless](images/navigate_serverless.png)

Looking at the file, you can see that we first define the name of the Knative service that will serve the function, and a brief description of it:

```yaml
service: functions
description: "Deploying functions from GitLab using Knative"
```

Next we can see the **provider** section defines the provider used to execute what's in the `serverless.yml` file. In this case, the TriggerMesh `tm` CLI.

```yaml
provider:
  name: triggermesh
```

And then we define the functions we will have, including the `source` directory, the `runtime` to execute each function with, and the `buildargs`:

```yaml
functions:
  hello:
    source: hello
    runtime: https://gitlab.com/gitlab-workshops/workshop-resources/knative-lambda-runtime/raw/master/python-3.7/buildtemplate.yaml
    description: "python Hello function with KLR template"
    buildargs:
     - DIRECTORY=hello
     - HANDLER=hello.endpoint
```
For the `buildargs` we are defining the `directory` that our function is in, in this case `hello`, and the `handler` which is the name of the method to call, in this case `hello.endpoint` (remember that method name we could have named "foo" instead? This is where you would have also changed that.)

That's it! Our serverless function has been set up with these two files. Now we just need to deploy it.

## GitLab CI Configuration

Now that we have a function and some information about it, the last thing we
need is a way to deploy it, and that's where GitLab CI comes in.

In the repository **root** directory view the `.gitlab-ci.yml` file:

![navigate_ciyaml](images/navigate_ciyaml.png)

Looking at the file, you can see that first we define a single stage in our pipeline, which is `deploy-function`:

```yaml
stages:
  - deploy-function
```

And then we define the job `deploy-hello-function` in the `deploy-function` stage, defining the `environment` we'll use, the container `image` to use, and the commands to run:

```yaml
deploy-hello-function:
  stage: deploy-function
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - echo $TMCONFIG > tmconfig
  script:
    - tm --config ./tmconfig deploy --wait; echo
```

Note that the `before_script` is creating a **tmconfig** file with some data which we've pre-loaded into each lab account. The data is am account speciic token to the TriggerMesh Cloud which we are deploying to.

The `script` is the main part of the job we are running. Here we are using the TriggerMesh CLI `tm`, with our config file, to deploy our function.

That's it! A super simple pipeline to deploy our function to the TriggerMesh Cloud.

## Run the pipeline

Normally, GitLab will run the pipeline for you on any commits, but since we've only been viewing files no pipelines have run yet and no deploys have been done. So let's go do that.

Exit the WebIDE by clicking on the project name in the upper left corner:

![navigate_pipelines_1](images/navigate_pipelines_1.png)

Then on the left-hand side bar, click on **CI/CD** -> **Pipelines**:

![navigate_pipelines_2](images/navigate_pipelines_2.png)

This will bring you to the **Pipelines** page where you can see all your pipelines for your project and their statuses (there are probably none right now). You can also manually start a new pipeline from this page, which is what we are going to do.

In the upper right corner of this page click on the green **Run Pipeline** button:

![run_pipeline_1](images/run_pipeline_1.png)

On the next screen leave the **Run for** set to `master` since we want to run the pipeline on our default branch called "master". Then click on the green **Run Pipeline** button:

![run_pipeline_2](images/run_pipeline_2.png)

This will bring you to the pipeline graph page for your new pipeline, which in this case has only one stage with one job in it. Click on the job to see the detailed job log:

![run_pipeline_3](images/run_pipeline_3.png)

In the job logs you might see some errors while things are spinning up. You can safely ignore those:

![run_pipeline_4](images/run_pipeline_4.png)

When the job completes, you will get some output that looks like the following. Copy your function's URL (in the second to last line) to your clipboard:

![run_pipeline_5](images/run_pipeline_5.png)

## Test the Function

To test out your function, open up a new broswer tab and make a request to the URL
you copied from the job output log. The page will load for a few seconds while a
container spins up to run your function.

Once it does, the output will look like:

```shell
{"message": "Hello, the current time is 22:39:43.706024"}
```

If your page does not load or you receive a different response, please raise
your hand and one of the workshop TA's will come by.

## Summary

So, what exactly did we do here?

1. Looked at a serverless function to return the time in UTC in a JSON blob.
1. Looked at a file which tells Knative which runtime your function uses, and
   what to call it when it is deployed.
1. Looked at a GitLab CI/CD pipeline configuration to deploy your function to Knative on
   TriggerMesh cloud.
1. Ran the GitLab CI/CD pipeline to deploy your function to TriggerMesh cloud.
1. Invoked your newly deployed function. On invocation, TriggerMesh cloud created a Kubernetes
   pod and a Kubernetes service to run your function in. This means that a
   container was created, filesystem laid down, and initialized with your
   python function as its initial process.
1. The function determined the current time in UTC and returned it to the
   caller (your browser).
1. Once the service has not received any more traffic over a certain interval,
   the container is destroyed, which means your time service has scaled down
   from 1 to 0.

## Congratulations!!

![Celebration Tanuki](lab1/images/celebrate-tanuki.png)

<p>

So, why would we care about this? Serverless functions are meant to run on an
as-needed basis. If part of your application relies on computations that do not
need to be running all the time, serverless functions provide an easy way to only spin
them up when you need them. This allows your infrastructure to only use resources when they are actually needed, and can prevent you from wasting CPU cycles.

We will demonstrate this further in the next lab. . .

## Proceed to Lab 2

Continue to [lab 2](../lab2/README.md)
